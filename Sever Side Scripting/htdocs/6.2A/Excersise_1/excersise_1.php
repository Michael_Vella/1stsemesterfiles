<!doctype html>
<html>
	<head>
		<title>Excersise 1 </title>
	</head>
	
	<body>
		<h1>Excesise Sheet 1</h1>
		<?php
			/*
				Number 1
			*/
			echo "<h2>Number 1</h2>";
			echo "hello world";
			/*
				Number 2
			*/
			$foo = True;
			$age = 21;
			$height = 1.68;
			echo "<hr/><h2>Number 2</h2>";
			echo "value of foo is :$foo </br>";
			echo "value of age is :$age </br>";
			echo "value of height is :$height meters</br>";
			/*
				Number 3
			*/
			echo "<hr/><h2>Number 3</h2>";
			echo "<p>Hello from the php compiler!</p>";
			/*
				Number 4
			*/
			echo "<hr/><h2>Number 4</h2>";
			$expand = 77.33;
			$either = 80;
			
			echo "Variabels $expand, $either";
			/*
				Number 5
			*/
			echo "<hr/><h2>Number 5</h2>";
			define("CONST1", 20);
			define("CONST2", 22);
			echo "CONSTANT1 ".CONST1."</br>";
			echo "CONSTANT2 ".CONST2."</br>";
			/*
				Number 6
			*/
			echo "<hr/><h2>Number 6</h2>";
			$name ="Sharon Needles";
			$age2 = 28;
			echo "Name: $name </br> Age: $age2";
			/*
				Number 7
			*/
			echo "<hr/><h2>Number 7</h2>";
			$numb1 = 5;
			$numb2 = 5;
			
			echo "$numb1 + $numb2 = ".($numb1 + $numb2)."</br>";
			echo "$numb1 - $numb2 = ".($numb1 - $numb2)."</br>";
			echo "$numb1 * $numb2 = ".($numb1 * $numb2)."</br>";
			echo "$numb1 / $numb2 = ".($numb1 / $numb2)."</br>";
			/*
				Number 8
			*/
			echo "<hr/><h2>Number 8</h2>";
			$a = 20;
			$b = 10;
			$c = $b - $a;
			echo "$c";
			/*
				Number 9
			*/
			echo "<hr/><h2>Number 9</h2>";
			$name = "Joseph";
			$surname = "Borg";
			$fullname = $name." ".$surname;
			echo "$fullname"; 
			/*
				Number 10
			*/
			echo "<hr/><h2>Number 10</h2>";
			define("PI", 3.142);
			$radius = 5;
			$answer = PI * ($radius * $radius);
			echo "$answer";
			/*
				Number 11
			*/
			echo "<hr/><h2>Number 11</h2>";
			$val = 50;//cm
			$inch = 1;
			$cmToInch= 2.54;
			$ans = $val/$cmToInch;
			echo "$ans";
		?>
	</body>
</html>