<!doctype html>
<html>
	<head>
		<title>Welcome to PHP</title>
	</head>
	<body>
		<h1>PHP</h1>
		
		<?php
			//php code goes here!
			
			echo "<p>Hello world!</p>";
			echo 'Hello again...!';
			echo "<br/>";
			echo "My name is \"Joe\"";
			echo "<br/>";
			echo 'My name is "Joe"';
			
			/*
				This is
				a multi-line
				comment
			*/
			
			
			echo "<h2>Strings</h2>";
			//strings
			$name = "Joe";
			$surname = "Borg";
			
			//int
			$age = 40;
			
			//booleans
			$sunny = true;
			$raining = false;
			
			//floating point number
			$price = 33.99;
			
			var_dump($name);
			echo "<hr/>";
			var_dump($age);
			echo "<hr/>";
			var_dump($sunny);
			echo "<hr/>";
			var_dump($price);
			
			echo "<h2>Constants</h2>";
			
			define("VAT_RATE", 0.18);
			//concatenating string "The current.." with VAT_RATE
			//concatenation character in PHP is . (not +)
			echo "The current vat rate is ".(VAT_RATE*100)."%";
			echo "<hr/>";
			
			///www.php.net/number_format
			$price_without_vat = number_format(45,2);
			$vat = number_format(VAT_RATE*$price_without_vat,2);
			$total_price = number_format($vat + $price_without_vat,2);
			
			echo "Price without VAT: &euro;".$price_without_vat;
			echo '<br/>VAT: &euro;'.$vat;
			echo "<br/>Total price: &euro;$total_price";
			
		?>
		
	</body>
</html>	