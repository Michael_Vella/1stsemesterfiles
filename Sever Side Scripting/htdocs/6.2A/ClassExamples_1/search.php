<!doctype html>
<html>
	<head>
		<title>Welcome to PHP</title>
	</head>
	<body>
		<h1>GET Superglobal</h1>
		
		<?php
			//if query is set in the URL, save the query inside $query variable
			//search.php?query=ANYTHING!
			if (isset($_GET['query'])) {
				$query = $_GET['query'];
				echo "Searching for $query....";
			}
			else {
				echo "No query set.";
			}
		?>
		
	</body>
</html>	