<!doctype html>
<html>
	<head>
		<title>Welcome to PHP</title>
	</head>
	<body>
		<h1>Arrays</h1>
		
		<?php
			//echo "<pre>";
			//print_r($_SERVER);
			//echo "</pre>";
			
			//Indexed array (numeric keys)
			//method 1 (using the array() function)
			$fruits = array("banana", "apples", "kiwi");
			
			//method 2 (also with numeric keys)
			$fruits2[0] = "banana";
			$fruits2[1] = "apples";
			$fruits2[2] = "kiwi";
			
			//associative array
			
			//country_name = Malta
			//population = 400000
			//lang = maltese
			//fav_food = pastizzi
			
			$countries = array(
							"mt" => array(
										"country_name" => "Malta", 
										"population" => 400000, 
										"lang" => "Maltese", 
										"fav_food" => "pastizzi"),
							"uk" => array(
										"country_name" => "United Kingdom", 
										"population" => 200000, 
										"lang" => "English", 
										"fav_food" => "fish and chips"),
							"it" => "Italy");
			
			echo "I live in ".$countries["mt"]["country_name"]." and visited ".$countries["uk"]["country_name"]." and ".$countries['it']." last summer...";
			
			$super5 = array(
						rand(1,45),
						rand(1,45),
						rand(1,45),
						rand(1,45),
						rand(1,45));
						
			echo "<hr/>";
			print_r($super5);
			echo "<hr/>";
			echo "Numbers: {$super5[0]}, {$super5[1]}, {$super5[2]}, {$super5[3]}, {$super5[4]}";
			
			echo "<h2>Date function</h2>";
			
			echo "Today is ".date("d-M-Y")."<br/>";
			
			//using php.net/date output the following:
			//Today is Tuesday 24th October 2017
			//The time is: 3:06pm
			
			echo "Today is ".date("l dS F Y")."<br/>";
			echo "The time is: ". date("g:ia")."<br/>";
			
			//time() - php.net/time
			$current_time = time();
			
			echo "Current time is: $current_time";
			
			//1 year in seconds
			$one_year = 60*60*24*365;
			
			//how many years in the current_time (time())
			$result = $current_time / $one_year;
			
			echo "<br/>years: $result";
			
			$an_hour_ago = $current_time - (60*60);
			echo "<br/>An hour ago it was ".date("g:ia", $an_hour_ago);
			
			echo "<hr/>";
			echo "<h2>User-defined Functions</h2>";
			function sum($a, $b) {
				$ans = $a + $b;
				return $ans;
			}
			
			$result = sum(10, 2);
			echo "The sum of 10 and 2 is $result<br/>";
			echo "The sum of 5 and 1 is ".sum(5,1);
			
			function outputDivision($number, $dividedby) {
				
				if ($dividedby == 0)
					return false;
				
				//you don't need to add else in this case because 'return false' will exit the function
				
				$divide = (int)($number / $dividedby);
				$remainder = $number % $dividedby;
				
				$return_array = array($divide, $remainder);
				
				return $return_array;
			}
			echo "<hr/>";
			
			$x = 5;
			$y = 2;
			$ans = outputDivision($x,$y);
			
			//if the return type is an array (so paramater 2 was not a zero)...
			if (gettype($ans) == "array") {
				//if $ans[1] (remainder) is not 0 (then there's a remainder)...
				if ($ans[1] != 0)
					echo "$x / $y = ".$ans[0]." r ".$ans[1];
				//if there is no remainder
				else
					echo "$x / $y = ".$ans[0];
			}
			//if the return type is not an array (so it must be a boolean (false))
			else {
				echo "Cannot divide by 0!!";
			}
			
		?>
		
	</body>
</html>	