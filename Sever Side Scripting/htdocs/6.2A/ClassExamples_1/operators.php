<!doctype html>
<html>
	<head>
		<title>Welcome to PHP</title>
	</head>
	<body>
		<h1>Operators</h1>
		
		<?php
			$x = 4;
			$y = 2;
			
			echo "$x - $y = ".($x-$y);
			echo "<hr/>";
			
			$addition = $x + $y;
			echo "$x + $y = $addition";
			echo "<hr/>";
			
			$a = 5;
			$b = 2;
			
			$remainder = $a % $b; //1
			$division = $a / $b; //2.5 -> 2
			$division_without_point = (int)$division; //cast the number to int (convert float to int)
			//floor() function would have worked as well in this case, but this is not ideal for negative numbers!
			
			echo "$a / $b = $division_without_point r $remainder";
			
			echo "<hr/>";
			echo $a++;
			echo "<br/>";
			echo $a;
			
			echo "<h2>Operators</h2>";
			$x = 7;
			$y = 5;
			$z = "7";
			
			$equal = ($x == $z);
			var_dump($equal);
			echo "<hr/>";
			
			$identical = ($x === $z);
			var_dump($identical);
			echo "<hr/>";
			
			echo "<h3>String operators</h3>";
			
			$full_name = "Joe";
			
			//short hand method of the below
			$full_name .= " Abela";
			//$full_name = $full_name . " Abela";
			
			echo "My name is $full_name<br/>";
			//this will not work;
			echo 'My name is $full_name<br/>';
			//but this will:
			echo 'My name is '.$full_name.'<br/>';
			
			echo "<p style=\"color:red\">Name: $full_name</p>";
			echo '<p style="color: red">Name: '.$full_name.'</p>';
			
			
		?>
		
	</body>
</html>	