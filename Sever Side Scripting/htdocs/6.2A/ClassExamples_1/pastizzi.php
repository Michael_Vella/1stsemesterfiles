<!doctype html>
<html lang="en">
	<head>
		<title>Hello, world!</title>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	</head>
	<body>	
		<div class="container">
			<div class="jumbotron text-center">
				<h1 class="display-3 ">Pastizzi Online</h1>
				<p class="lead">The best!</p>
				<img src="http://www.maltabulb.com/images/easy-cheesecake-recipes.jpg" class="img-fluid" style="border-radius: 50%" />
			</div>
			
			<?php
				if (isset($_POST['submit_btn'])) {
					//echo '<div class="alert alert-info">Form was submitted</div>';
					
					$pizelli = intval($_POST['pizelli']);
					$irkotta = intval($_POST['irkotta']);
					
					define("PIZELLI_COST", 0.30);
					define("IRKOTTA_COST", 0.28);
					
					$pizelli_cost = 0;
					$irkotta_cost = 0;
					$total_price = 0;
					$breakdown = "";
					
					if ($pizelli >= 0 || $irkotta >= 0) {
						
						if ($pizelli < 0 || $irkotta < 0 || $pizelli > 300 || $irkotta > 300 || ($pizelli == 0 && $irkotta == 0)) {
							echo '<div class="alert alert-warning"><h3>Nice try!</h3> Cajta tajba, sabiħa!</div>';
						}
						else {
							
							if ($pizelli > 0) {
								$pizelli_cost = $pizelli * PIZELLI_COST; 
								$breakdown .= "Pizelli - QTY: $pizelli @ &euro;".number_format(PIZELLI_COST,2)." each = &euro;".number_format($pizelli_cost,2)."<hr/>";
							}
							if ($irkotta > 0) {
								$irkotta_cost = $irkotta * IRKOTTA_COST; 
								$breakdown .= "Irkotta - QTY: $irkotta @ &euro;".number_format(IRKOTTA_COST,2)." each = &euro;".number_format($irkotta_cost,2)."<hr/>";
							}
							
							$total_price = number_format($pizelli_cost + $irkotta_cost,2);
							$breakdown .= "<strong>Total price: &euro;$total_price</strong>";
							
							echo '<div class="alert alert-success"><h3>Thank you for your order!</h3>'.$breakdown.'</div>';
						}
					}
					else
						echo '<div class="alert alert-warning"><h3>Erm?!</h3> You need to place your order...!</div>';
					
				}
			
			?>
			
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<form method="post" action="pastizzi.php">
						<div class="row form-group">
							<div class="col-sm-2">
								<label for="pizelli">Pizelli</label>
							</div>
							<div class="col-sm-4">
								<input type="number" class="form-control" id="pizelli" name="pizelli" aria-describedby="pizelliHelp" placeholder="Qty">
							</div>
							<div class="col-sm-4">
								<small id="pizelliHelp" class="form-text text-muted">@ &euro;0.30c each</small>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label for="irkotta">Irkotta</label>
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="irkotta" name="irkotta" aria-describedby="irkottaHelp" placeholder="Qty">
							</div>
							<div class="col-sm-4">
								<small id="irkottaHelp" class="form-text text-muted">@ &euro;0.28c each</small>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-10">
								<button type="submit" name="submit_btn" class="btn btn-block btn-danger">Buy!</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			
		</div>
		
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	</body>
</html>