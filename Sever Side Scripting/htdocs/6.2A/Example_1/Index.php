<!doctype html>
<html>
    <head>
        <title>Welcome to PHP</title>
    </head>
    <body>
        <h1>
            PHP
        </h1>
        
        <?php
            //php code goes here
            echo "Hello World!!";
            print "Hello again";
            echo "\Henlo\"";
            
            echo "<h2>Strings</h2>";
            //Strings
            $name = "Joe";
            $surname = "Borg";

            echo "<h2>Integer</h2>";
            //int
            $age = 14;
            
            echo "<h2>Booleans</h2>";
            //boolean
            $sunny = true;
            $raining = false;
                
            echo "<h2>Floating point numbers</h2>";
            //floasting point number
            $price = 33.99;
            
            //function is for procidural languages. Method should be refered for functions within a class
            
            var_dump($name);
            var_dump($surname);
            var_dump($age);
            var_dump($sunny);
            var_dump($raining);
            var_dump($price);
            
            echo "<h2>Constants</h2>";
            define("VAT_RATE", 0.18);
            
            //Concatinating the string below with the constant
            //The symbol of concatenation is '.' not '+'
            echo "Current Vate Rate:".(VAT_RATE*100)."%";
        
            $price_without_vat = 45;
            echo "<br>Price without VAT: ".$price_without_vat;
            $with_vat = (VAT_RATE*$price_without_vat) + $price_without_vat;
            echo "<br>Total Price: ".$with_vat;
        ?>
    </body>
</html>