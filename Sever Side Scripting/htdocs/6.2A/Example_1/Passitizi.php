<!doctype html>
<html lang="en">
  <head>
    <title>Using_Post</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
  <div class="jumbotron text-center">
	 <h1>Using_Post</h1>
  </div>
   
		<div class="from-group" >
		<form action="Passitizi.php" method="post">
			<label for="ricotta">Ricotta @ 0.30 cents</label><input type ="text" name="ricotta" placeholder="amount_ricotta"/>
			<br/><br/>
			<label for="pizzelli">Pizelli @ 0.50 cents</label><input type="text" name="pizzelli" placeholder="amount_pizzelli"/>
			<br/><br>
			<input type="submit" name="submit_btn" value="BUY!"/>
		</form>
		</div>
    
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<?php
			$RIC_PRICE = 0.30;
			$PIZ_PRICE = 0.50;
			
			if(isset($_POST['submit_btn']))
			{
				$ricotta = $_POST['ricotta'];
				$pizzelli = $_POST['pizzelli'];		
				
				$ric_amount = (int)$ricotta;
				$piz_amount = (int)$pizzelli;
				
				$total_pizzelli = $piz_amount * $PIZ_PRICE;
				$total_ricotta = $ric_amount * $RIC_PRICE;
				if(($ric_amount >= 0) && ($piz_amount >= 0))
				{
					echo "Ricotta @ $RIC_PRICE cents * $ric_amount<br/>";
					echo "Pizelli @ $PIZ_PRICE cents * $piz_amount<br/>";
					$total_pizzelli = $piz_amount * $PIZ_PRICE;
					$total_ricotta = $ric_amount * $RIC_PRICE;
					
					echo 'Total Cost: '.number_format($total_pizzelli + $total_ricotta,2);
					
				}else if(!empty($piz_amount >= 0))
				{
					echo "Pizelli @ $PIZ_PRICE cents * $piz_amount<br/>";
					$total_pizzelli = $piz_amount * $PIZ_PRICE;
					
					echo 'Total Cost: '.number_format($total_pizzelli,2);
					
				}else if(!empty($ric_amount >= 0))
				{
					echo "Ricotta @ $RIC_PRICE cents * $ric_amount<br/>";
					$total_ricotta = $ric_amount * $RIC_PRICE;
					echo 'Total Cost: '.number_format($total_ricotta,2);
				}else 
				{
					echo "NOPE";
				}

			}else
			{
				echo "You didnt buy anything";
			}
		?>
  </body>
</html>