<!doctype html>
<html>
	<head>
		<title>Welcome to PHP</title>
	</head>
	<body>
		<h1>Control Structures</h1>
		
		<h2>While Loops</h2>
		<?php
			
			$i = 1;
			
			while ($i >= -4) {
				echo $i--."<br/>";
			}
			
			
			$x = 1;
			echo "<hr/>";
			while ($x <= 12) {
				echo $x." x 5 = ".($x*5)."<br/>";
				$x++;
			}
			
			//re-create the super5 rand() example
			//using a while loop, add 5 random numbers to an array $super5
			
			//$somearray = array("hello", "world");
			
			//$somearray[] = "lalala"; 
			//append to array $somearray
			
			$super5 = array();
			$y = 1;
			echo "<hr/>";
			
			while ($y <= 5) {
				do {
					$random_number = rand(1,45);
					$exists = in_array($random_number, $super5);
	
					if (!$exists) {
						$super5[] = $random_number;
						echo "Number $random_number doesn't exist...so we add it<br/>";
					}
					else {
						echo "Number $random_number exists so we do not add it...<br/>";
					}
				} while ($exists == true);
				
				$y++;
			}
			echo "<hr/>";
			
			$y = 1;
			$super52 = array();
			//or
			while ($y <= 5) {
				$random_number = rand(1,5);
				$exists = in_array($random_number, $super52);
	
				if (!$exists) {
					$super52[] = $random_number;
					echo "Number $random_number doesn't exist...so we add it<br/>";
					
					//increment only if the number doesn't exist!!
					$y++;
				}
				else {
					echo "Number $random_number exists so we do not add it...<br/>";
				}
			}
			
			echo "<pre>";
			print_r($super5);
			print_r($super52);
			echo "</pre>";
			
			echo "<hr/>";
			
			for ($i = 1; $i <= 10; $i++) {
				echo $i."<br/>";
			}
			
			echo"<hr/> <h2> Foreach loops </h2>";
			foreach($super5 as $number) 
			{
				echo $number."<br/>";
			}
			
			$countries = array("mt" => "Malta", "uk" => "United Kingdom", "it"=>"Italy");
			foreach ($countries as $key => $value)
			{
				echo "country code: ".$key.", Country Name: ".$value."<br/>";
			}
			
			
		?>
		
	</body>
</html>	