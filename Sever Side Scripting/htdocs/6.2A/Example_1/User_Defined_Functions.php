<!doctype html>
<html lang="en">
  <head>
    <title>User Defined Functions</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
  <div class="jumbotron text-center">
	 <h1>User Defined Functions</h1>
 </div>
    
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<?php		
		 function sum($a,$b)
		 {
			 $ans = $a + $b;
			 return $ans;
		 }
		 $result = sum(10,20);
		 echo "The sum of 10 and 20 is ".$result."</br>";
		 echo "The sum of 10 and 20 is ".sum(10,20)."</br>";
		 
		 echo "</hr>";
		 function outputDivision($a,$b)
		 {
			 if($b>0){
				$divide = (int)($a/$b);
				$remainder = $a%$b;
				$array = array($divide, $remainder);
				return $array;
			 }
			 else
			 {
				 return false;
			 }
		 }
		 $ans = outputDivision(5,2);
		 if(gettype($ans)=="array")
		 {
			 if($ans[1] !=0);
				echo "Numbers divided by equal to :".$ans[0]." remainder: ".$ans[1]."</br>";
		 }
		 
		?>
  </body>
</html>