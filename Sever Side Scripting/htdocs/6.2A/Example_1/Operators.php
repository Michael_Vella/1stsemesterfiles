<!doctype html>
<html>
    <head>
        <title> Welcome to PHP</title>
    </head>
    <body>
        <h1>Operators</h1>
        <?php
            $x = 4;
            $y = 2;
            
            echo "$x - $y = ".($x-$y);
            echo "<hr/>";
        
            $addition = $x + $y;
            echo "$x + $y = $addition";
            echo "<hr/>";
                
            $a = 5;
            $b = 2;
        
            $division = $a/$b;
            echo "$a / $b =".number_format($division,0);
            echo "<hr/>";
            
            $mod = $a%$y;
            echo "$a % $y = ".number_format($mod,2);           
            echo "<hr/>";
            
			echo "<h2>Operators</h2>";
			$x = 7;
			$y = 5;
			$z = 7;
			
			$equal = ($x == $y );
			var_dump($equal);
			echo "<hr/>";
			
			$identical  = ($x === $y);
			var_dump ($identical);
			echo "<hr/>";
			
			echo "<h3>String Operators </h3>";
			$full_name = "Joe";
			
			//Short hand method of the below 
			$full_name .=" Ablea";
			
			//full_name = $full_name ." Abela";
			
			echo "My name is $full_name<br/>";
			
			//this will not work;
			echo 'My name is $full_name<br/>';
			echo 'My name is '.$full_name.'<br/>';
			echo "<p style=\"color:red\">Name: $full_name</p>";
			echo '<p style="color:red">Name:'.$full_name.'</p>';
		?>
    </body>
</html>