<!doctype html>
<html lang="en">
  <head>
    <title>Internal Functions of PHP</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>
  <div class="jumbotron text-center">
	 <h1>Internal Functions of PHP</h1>
 </div>
    
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<?php
		//echo "<pre>";
		//print_r($_SERVER);
		//echo "</pre>";
		//method 1
		$array = array(Rand(1,20),Rand(1,20),Rand(1,20),Rand(1,20),Rand(1,20),Rand(1,20));
		print_r($array);
		
		echo "<br>Numbers: {$array[0]}, {$array[1]}, {$array[2]}, {$array[3]}, {$array[4]}";
		
		echo "<hr><h2> Date Function </h2>";
		echo "Today is ".date("d-m-y")."<br/>";
		
		//using php.net/date outputs the following: Todat is Tuesday 24th October 
		//The time is: 3:08
		
		echo  "Today is ".date("l dS F Y")."<br/>";
		echo  "Time is is ".date("g:ia")."<br/>";
		
		//time() - php.net/time
		$current_time = time();
		echo  "The Current Time is (in seconds) ".$current_time."<br/>";
		
		//1 year in seconds 
		$one_year =60 * 60 *24*365;
		
		//how many years there are in the current time stamp 
		$result =  $current_time/$one_year;
		
		echo "Years since UNIX Epoch: $result<br/>";
		
		$an_hour_ago = $current_time -(60*60);
		echo "An hour ago it was ".date("g:ia",$an_hour_ago)."<br/>";
		?>
  </body>
</html>