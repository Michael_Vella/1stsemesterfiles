#include <stdio.h>
#include <stdlib.h>

char c;

void do_user_input()
{
	//malloc measures in bytes 
	char* buffer = (char*)malloc(128);
	int number = 0;

	printf("Enter a word: ");
	scanf("%s", buffer);

	printf("Enter a number: ");
	scanf("%d", &number);
	//C is set to a character from the buffer (string) if it doesnt over flow
	c = buffer[number];
	printf("Character at %d = %c: \n", number, c);
	//Buffer is freed from space
	free(buffer);
}

//Note running the file will generate a segmentation fault (ran out of memeory) if the buffer takes in a word that is too large
int main(int argc, char **argv)
{
	do_user_input();
	return 0;
}
